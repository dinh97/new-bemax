$(document).ready(function(){
    $('a.btn-play').on('click', function(event) {
    	event.preventDefault();
    	$(this).parent().hide();
    	$(this).parent().parent().find('#thevideo').show();
    	$(this).parent().parent().find('#thevideo-1').show();
    	$(this).parent().parent().find('#thevideo').find('#someFrame').attr("src", $(this).attr("href"));
    	$(this).parent().parent().find('#thevideo-1').find('#someFrame').attr("src", $(this).attr("href"));
    });
});