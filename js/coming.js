$(function(){
    $('#clock').countdown('2020/10/10', function(event) {
        var $this = $(this).find('>ul').html(event.strftime(''
          + '<li><span>%d</span><br> days </li>'
          + '<li><span>%H</span><br> hours </li>'
          + '<li><span>%M</span><br> minutes </li>'
          + '<li><span>%S</span><br> seconds</li>'));
    });
});