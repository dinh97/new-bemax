(function ($) {
      // USE STRICT
      "use strict";
      try {
            $('.testi-slick').slick({
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  arrows: false,
                  fade: true,
                  dots: false,
                  asNavFor: '.testi-slick-nav',
                  initialSlide: 2,
                  autoplay: false,
            });
            $('.testi-slick-nav').slick({
                  slidesToShow: 5,
                  slidesToScroll: 1,
                  asNavFor: '.testi-slick',
                  dots: false,
                  centerMode: true,
                  focusOnSelect: true,
                  initialSlide: 2,
                  responsive: [{
                        breakpoint: 575,
                        settings: {
                              slidesToShow: 3,
                              autoplay: true,
                        }
                  }]
            });
      } catch (err) {
            console.log(err);
      }
})(jQuery);