(function ($) {
	// USE STRICT
	"use strict";
	try {
		var $grid = $('.grid').isotope({
			// main isotope options
			itemSelector: '.port-item',
			// set layoutMode
			layoutMode: 'masonry',

		});
		$('.port-filter').on('click', 'li', function () {
			$('.port-filter li').removeClass('active');
			$(this).addClass('active');
			var filterData = $(this).attr('data-filter');
			$grid.isotope({
				filter: filterData
			});
		});


		$('.port-item').hover(function () {
			$(this).children('.port-item-detail').toggleClass('show', 400);
		});
	} catch (err) {
		console.log(err);
	}
})(jQuery);