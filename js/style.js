/* Loading ....*/

$(window).on('load', function(){
	$('.loading').fadeOut();
})

/* Header*/

$(document).ready(function(){
	$("nav.desktop-menu li").hover(function() {
		$(this).find(">ul").slideDown(350);
	}, function() {
		$(this).find(">ul").slideUp(250);
	});

	$('nav.desktop-menu li > a').on('click', function(){
		$(this).parent().find("> ul").slideToggle(400);
	});
	$('.bt-search > i').on('click',function(){
		$('.search-model').fadeIn(500);
	});

	$('.close-search').on('click',function(){
		$('.search-model').fadeOut(500);
	});
	

    $(".desktop-menu > ul").children().clone().appendTo($("#mobile-menu > ul")), $("#show-mobile-menu").on("click", function() {
        $(this).toggleClass("open"), $(".mobile-menu > ul").stop(true, true).slideToggle()
    }); 

    $("nav.mobile-menu > a").on("click", function() {
        $(this).parent().find("> ul").slideToggle();
    });

    $("nav.mobile-menu > ul li > a").on("click", function() {
        $(this).parent().find("> ul").slideToggle()
    }); 

    $('.bt-bar').on('click',function(event) {
    	$('.menu-bar').toggleClass('open');
    });

    $(window).resize(function(event) {
    	if($(window).width() >768){
	    	$('nav.mobile-menu > ul').hide();
	    }
    });
	stickyHeader();
	switchColor();
});

function stickyHeader(){
	var t = $('header .header-container');
	t.after("<div class='header__holder'></div>");
	var pos = t.outerHeight();
	var win = $(window);
	$(window).on('scroll', function(){
		if(win.scrollTop() >= pos){
			t.addClass('pre-sticky');
			$('.header__holder').css("height",pos);
		}
		if(win.scrollTop() >= pos+10){
			t.addClass('sticky');
		}
		else{
			t.removeClass('sticky');
			t.removeClass('pre-sticky');
			$('.header__holder').css("height",0);
		}
	});
}

function showMobileMenu(){

	$("#show-mobile-menu").on('click', function(){
		$('nav.mobile-menu').show();
		$('nav.mobile-menu > ul').slideToggle(400);
	});

	$('nav.mobile-menu a').on('click',function(){
		$(this).parent().find("> ul").slideToggle(400);
	});
}