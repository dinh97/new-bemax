(function ($) {
	// USE STRICT
	"use strict";
	try {
		$('.counter').counterUp({
			time: 1000
		});
		$('.counter-skill').counterUp({
			time: 1000
		});
	} catch (err) {
		console.log(err);
	}
})(jQuery);