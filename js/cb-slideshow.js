
(function ($) {
	// USE STRICT
	"use strict";
	try {
		$('.cb-slideshow li').each(function () {
			var urlImage = "url('" + $(this).attr('data-image') + "')";
			console.log(urlImage);
			$(this).css({
				backgroundImage: urlImage,
			});
		});

		$('.cb-slideshow').parent().css({
			position: 'relative',
		});
	} catch (err) {
		console.log(err);
	}
})(jQuery);