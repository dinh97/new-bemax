function switchColor(){
	$('#switch-color').on('click', function(event) {
		$('.color').toggleClass('show');
	});
	$('#list-colors li').on('click', function(event) {
		var color = $(this).attr('class');
		var currentColor = $('#color').attr("href") != null ? $('#color').attr("href") : "css/color/blue.css";

		currentColor = currentColor.split("css/color/");
		var text = currentColor[1];
		text = text.split(".css");
		$('.bg-' + text[0]).addClass('bg-' + color).removeClass('bg-' + text[0]);
		$('#color').attr("href","css/color/" + color + ".css");
	});
}