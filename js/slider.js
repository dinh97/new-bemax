(function ($) {
	// USE STRICT
	"use strict";
	try {
		$('#slide-1').revolution({
			slideLayout: 'fullscreen',
			delay: '6000',
			autoPlay: 'off',
			disableProgressBar: 'on',
			navigation: {
				arrows: {
					enable: true,
					hide_onleave: false
				},
				bullets: {
					enable: true,
					h_align: 'center',
					v_align: 'bottom',
					hide_onleave: false,
					h_offset: 0,
					v_offset: 40,
					space: 10,
				}
			},
			responsiveLevels: [1240, 1024, 778, 440],
			gridheight: [590, 590, 590, 590],
		});


		$('#slide-2').revolution({
			slideLayout: 'fullscreen',
			delay: '5000',
			autoPlay: 'off',
			disableProgressBar: 'on',
			navigation: {
				arrows: {
					enable: true,
					hide_onleave: true,
				},
				bullets: {
					enable: true,
					h_align: 'center',
					v_align: 'bottom',
					hide_onleave: false,
					h_offset: 0,
					v_offset: 40,
					space: 10,
				}
			},
			responsiveLevels: [1240, 1024, 778, 440],
			gridheight: [590, 590, 590, 590],
		});

		$('#slide-3').revolution({
			slideLayout: 'fullscreen',
			delay: '6000',
			autoPlay: 'off',
			disableProgressBar: 'on',
			navigation: {
				arrows: {
					enable: true,
					hide_onleave: false
				},
				bullets: {
					enable: true,
					tmp: '<span class="tp-bullet-inner"></span>',
					h_align: 'center',
					v_align: 'bottom',
					hide_onleave: false,
					h_offset: 0,
					v_offset: 40,
					space: 10,
				}
			},
			responsiveLevels: [1240, 1024, 778, 440],
			gridheight: [590, 590, 590, 590],
		});


		$('#slide-4').revolution({
			slideLayout: 'fullscreen',
			delay: '6000',
			autoPlay: 'off',
			disableProgressBar: 'on',
			navigation: {
				arrows: {
					enable: false,
					hide_onleave: false
				},
				bullets: {
					enable: true,
					h_align: 'center',
					v_align: 'bottom',
					hide_onleave: false,
					h_offset: 0,
					v_offset: 40,
					space: 10,
				}
			},
			responsiveLevels: [1240, 1024, 778, 440],
			gridheight: [590, 590, 590, 590],
		});

		$('#slide-5').revolution({
			slideLayout: 'fullscreen',
			delay: '6000',
			autoPlay: 'off',
			disableProgressBar: 'on',
			navigation: {
				arrows: {
					enable: false,
					hide_onleave: false
				},
				bullets: {
					enable: true,
					h_align: 'center',
					v_align: 'bottom',
					hide_onleave: false,
					h_offset: 0,
					v_offset: 40,
					space: 10,
				}
			},
			responsiveLevels: [1240, 1024, 778, 440],
			gridheight: [705, 700, 590, 650],
		});
	} catch (err) {
		console.log(err);
	}
})(jQuery);