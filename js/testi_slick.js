(function ($) {
    // USE STRICT
    "use strict";
    try {
        $('.testi-slick').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            dots: false,
            asNavFor: '.testi-slick-nav',
            initialSlide: 2,
            autoPlay: false,
        });
        $('.testi-slick-nav').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.testi-slick',
            dots: false,
            centerMode: true,
            focusOnSelect: true,
            initialSlide: 2,
            autoPlay: false,
        });
    } catch (err) {
        console.log(err);
    }
})(jQuery);