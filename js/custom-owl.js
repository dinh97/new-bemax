$(document).ready(function () {

});


(function ($) {
	// USE STRICT
	"use strict";
	try {
		var owl_1 = $('.owl-1');
		var owl_2 = $('.owl-2');
		var owl_3 = $('.owl-3');
		var owl_4 = $('.owl-4');
		var owl_5 = $('.owl-5');
		var owl_6 = $('.owl-6');
		var owl_7 = $('.owl-7');
		var owl_8 = $('.owl-8');
		var owl_9 = $('.owl-9');
		var owl_10 = $('.owl-10');


		owl_1.owlCarousel({
			items: 1,
			loop: true,
			dots: false,
			nav: false,
		});

		owl_2.owlCarousel({
			items: 4,
			animate: 'fade',
			loop: true,
			nav: false,
			dots: false,
			autoplay: true,

			responsive: {
				0: {
					items: 1,
				},
				576: {
					items: 2,
				},
				768: {
					items: 3,
				},
				992: {
					items: 4,
				}
			}
		});

		owl_3.owlCarousel({
			items: 5,
			animateOut: 'fadeOutLeft',
			animateIn: 'fadeInRight',
			smartSpeed: 500,
			loop: true,
			nav: false,
			dots: false,
			autoplay: true,
			responsive: {
				0: {
					items: 1
				},
				500: {
					items: 2
				},
				576: {
					items: 3
				},
				768: {
					items: 4,
				},
				992: {
					items: 5,
				}
			}
		});

		owl_4.owlCarousel({
			items: 1,
			loop: true,
			nav: false,
			dots: true,
			center: true
		});

		owl_5.owlCarousel({
			items: 2,
			loop: true,
			dots: false,
			nav: false,
			responsive: {
				0: {
					items: 1
				},
				767: {
					items: 1
				},
				768: {
					items: 2
				},
				992: {
					items: 2
				}
			}
		});


		owl_6.owlCarousel({
			items: 6,
			loop: true,
			autoplay: true,
			nav: false,
			dots: true,
			responsive: {
				0: {
					items: 2,
				},
				576: {
					items: 3,
				},
				768: {
					items: 4,
				},
				992: {
					items: 6,
				}
			}
		});

		owl_7.owlCarousel({
			items: 3,
			loop: true,
			// autoplay: true,
			dots: false,
			nav: false,
			responsive: {
				0: {
					items: 1
				},
				576: {
					items: 2
				},
				768: {
					items: 2,
				},
				992: {
					items: 3
				}

			}
		});

		owl_8.owlCarousel({
			items: 6,
			loop: true,
			autoplay: true,
			nav: false,
			dots: false,
			responsive: {
				0: {
					items: 2,
				},
				576: {
					items: 3,
				},
				768: {
					items: 4,
				},
				992: {
					items: 6,
				}
			}
		});

		owl_9.owlCarousel({
			items: 4,
			loop: true,
			nav: false,
			dots: true,
			responsive: {
				0: {
					items: 1,
				},
				576: {
					items: 2
				},
				768: {
					items: 3,
				},
				992: {
					items: 4
				}
			}
		});

		owl_10.owlCarousel({
			items: 2,
			loop: true,
			dots: true,
			nav: false,
			responsive: {
				0: {
					items: 1
				},
				767: {
					items: 1
				},
				768: {
					items: 2
				},
				992: {
					items: 2
				}
			}
		});

		$('.owl-carousel').each(function () {
			var $carousel = $(this);
			$carousel.owlCarousel({
				dots: $carousel.data("dots"),
				items: $carousel.data("items"),
				slideBy: $carousel.data("slideby"),
				center: $carousel.data("center"),
				loop: $carousel.data("loop"),
				margin: $carousel.data("margin"),
				nav: $carousel.data("nav"),
				autoplay: $carousel.data("autoplay"),
				autoplayTimeout: $carousel.data("autoplay-timeout"),
				navText: ['<span class="fa fa-chevron-left"><span>', '<span class="fa fa-chevron-right"></span>']
			});
		});


		$('.owl-next').click(function () {
			$(this).parent().parent().parent().find('.owl-carousel').trigger('next.owl.carousel');
		});


		$('.owl-pre').click(function () {
			$(this).parent().parent().parent().find('.owl-carousel').trigger('prev.owl.carousel');
		});

		$('.owl-container').hover(function () {
			$(this).find('.owl-button > button').addClass('show');
		}, function () {
			$(this).find('.owl-button > button').removeClass('show');
		});
	} catch (err) {
		console.log(err);
	}
})(jQuery);